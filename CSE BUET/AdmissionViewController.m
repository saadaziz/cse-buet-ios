//
//  AdmissionViewController.m
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import "AdmissionViewController.h"

@interface AdmissionViewController ()

@end

@implementation AdmissionViewController
{
    NSString *str1,*str2;
}
@synthesize mytextview;
@synthesize mysegmentedcontrol;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Admission";
     str1=@"Bsc students info";
    
     str2=@"Post grad students info";
    mytextview.text=( [mysegmentedcontrol selectedSegmentIndex ] ==0)?str1:str2;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onSegmentedControlValueChanged:(id)sender
{
    NSLog(@"segment changed");
    int index = [mysegmentedcontrol selectedSegmentIndex ];
    mytextview.text=( index ==0)?str1:str2;
}
@end
