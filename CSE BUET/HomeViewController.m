//
//  HomeViewController.m
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import "HomeViewController.h"
#import "NoticeViewController.h"

#import "WebsiteViewController.h"
#import "AdmissionViewController.h"
#import "AboutViewController.h"
#import "FacultyViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"CSE BUET";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)onNoticeButtonClick:(id)sender
{
    NSLog(@"Notice button clicked");
    NoticeViewController *noticeViewController=[[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
   
    [self.navigationController pushViewController:noticeViewController animated:YES];
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@"Home"
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
}
-(IBAction)onAdmissionButtonClick:(id)sender
{
    NSLog(@"Admission button clicked");
    AdmissionViewController *adViewController=[[AdmissionViewController alloc] initWithNibName:@"AdmissionViewController" bundle:nil];
    
    [self.navigationController pushViewController:adViewController animated:YES];
    
    self.navigationController.navigationBar.backItem.title=@"Home";
}
-(IBAction)onFacButtonClick:(id)sender
{
    NSLog(@"Faculty button clicked");
    FacultyViewController *wViewController=[[FacultyViewController alloc] initWithNibName:@"FacultyViewController" bundle:nil];
    
    
    [self.navigationController pushViewController:wViewController animated:YES];
    
    self.navigationController.navigationBar.backItem.title=@"Home";
}
-(IBAction)onWebsiteButtonClick:(id)sender
{
    NSLog(@"Website button clicked");
    WebsiteViewController *wViewController=[[WebsiteViewController alloc] initWithNibName:@"WebsiteViewController" bundle:nil];

    
    [self.navigationController pushViewController:wViewController animated:YES];
    
    self.navigationController.navigationBar.backItem.title=@"Home";
}
-(IBAction)onAboutButtonClick:(id)sender
{
    NSLog(@"About button clicked");
    AboutViewController *abtViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    
    [self.navigationController pushViewController:abtViewController animated:YES];

    self.navigationController.navigationBar.backItem.title=@"Home";
}
-(IBAction)onContactButtonClick:(id)sender
{
    NSLog(@"Contact button clicked");
}
@end
