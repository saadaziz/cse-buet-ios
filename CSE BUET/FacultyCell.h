//
//  FacultyCell.h
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacultyCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UIImageView *photo;
@property(nonatomic,weak) IBOutlet UILabel *Name;
@property(nonatomic,weak) IBOutlet UILabel *Designation;
@property(nonatomic,weak) IBOutlet UILabel *Interest;
@end
