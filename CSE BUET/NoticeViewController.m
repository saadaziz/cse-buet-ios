//
//  NoticeViewController.m
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import "NoticeViewController.h"
#import "NoticeCell.h"
@interface NoticeViewController ()

@end

@implementation NoticeViewController{
    NSMutableArray *titles,*dates;
}
@synthesize mytableview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"Notice Board";
    titles = [[NSMutableArray alloc]init];
    dates = [[NSMutableArray alloc]init];
    [titles addObject:@"IOS workshop"];
    [titles addObject:@"Windows workshop"];
    [titles addObject:@"Android workshop"];
    [titles addObject:@"Cloud workshop"];
    [titles addObject:@"ACM workshop"];
    [titles addObject:@"IEEE workshop"];
    [titles addObject:@"Java workshop"];
    [titles addObject:@"Clojure workshop"];
    
    [titles addObject:@"Node.js workshop"];
    [titles addObject:@"osX workshop"];
    [titles addObject:@"Linux workshop"];
    
    [dates addObject:@"15th november, 2013"];
    [dates addObject:@"20th november, 2013"];
    [dates addObject:@"25th november, 2013"];
    [dates addObject:@"27th november, 2013"];
    [dates addObject:@"28th november, 2013"];
    
    [dates addObject:@"3rd december, 2013"];
    [dates addObject:@"30th december, 2013"];
    [dates addObject:@"31th January, 2013"];
    [dates addObject:@"29th november, 2014"];
    [dates addObject:@"30th november, 2014"];
    [dates addObject:@"31th november, 2014"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [titles count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell";
    
    NoticeCell *cell =(NoticeCell*) [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        NSArray *nibs= [[NSBundle mainBundle] loadNibNamed:@"NoticeCell" owner:self options:nil];
        
        cell=[nibs objectAtIndex:0];
    }
    cell.noticeTitle.text=[titles objectAtIndex:indexPath.row];
    cell.noticeDate.text=[dates objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   //
    
}
@end
