//
//  AdmissionViewController.h
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdmissionViewController : UIViewController
{}
@property (nonatomic,strong) IBOutlet UITextView *mytextview;
@property(nonatomic,strong) IBOutlet UISegmentedControl *mysegmentedcontrol;
-(IBAction)onSegmentedControlValueChanged:(id)sender;
@end
