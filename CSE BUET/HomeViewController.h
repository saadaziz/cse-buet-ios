//
//  HomeViewController.h
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
{}
-(IBAction)onNoticeButtonClick:(id)sender;
-(IBAction)onAdmissionButtonClick:(id)sender;
-(IBAction)onFacButtonClick:(id)sender;
-(IBAction)onWebsiteButtonClick:(id)sender;
-(IBAction)onAboutButtonClick:(id)sender;
-(IBAction)onContactButtonClick:(id)sender;
@end
