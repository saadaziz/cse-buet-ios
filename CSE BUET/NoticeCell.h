//
//  NoticeCell.h
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *noticeTitle;
@property(nonatomic,weak) IBOutlet UILabel *noticeDate;
@end
