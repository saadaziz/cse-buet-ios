//
//  FacultyViewController.h
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacultyViewController : UITableViewController
-(void) DownloadFacultyData;
-(void) LoadData:(NSArray*) data;
@end
