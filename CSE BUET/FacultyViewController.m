//
//  FacultyViewController.m
//  CSE BUET
//
//  Created by CSEBUET on 11/22/13.
//  Copyright (c) 2013 CSEBUET. All rights reserved.
//

#import "FacultyViewController.h"
#import "FacultyCell.h"
#import "SVHTTPRequest.h"
@interface FacultyViewController ()

@end

@implementation FacultyViewController
{
    NSMutableArray *tableDataArray;
}
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self DownloadFacultyData ];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableDataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"FacultyCell";
    FacultyCell *cell =(FacultyCell*)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil)
    {
        NSArray *nibs= [[NSBundle mainBundle] loadNibNamed:@"FacultyCell" owner:self options:nil];
        cell = [nibs objectAtIndex:0];
    }
    NSDictionary *celldata = [tableDataArray objectAtIndex:indexPath.row];
    // Configure the cell...
    cell.Name.text=[celldata objectForKey:@"name"];
    cell.Interest.text=[celldata objectForKey:@"research_area"];
    cell.Designation.text=[celldata objectForKey:@"designation"];
    //cell.photo.image= [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURLURLWithString:[celldata objectForKey:@"image_link"]]]];
    
    [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[celldata objectForKey:@"image_link"]]]];
//    cell.photo.image = nil;
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^(void) {
//        
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString[celldata objectForKey:@"image_link"]]];
//                                                   
//                                                   UIImage* image = [[UIImage alloc] initWithData:imageData];
//                                                   if (image) {
//                                                       dispatch_async(dispatch_get_main_queue(), ^{
//                                                           if (cell.tag == indexPath.row) {
//                                                               cell.photo.image = image;
//                                                               [cell setNeedsLayout];
//                                                           }
//                                                       });
//                                                   }
//                                                   });
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}
-(void) LoadData:(NSArray*) data{
    if(data)
    {
        for(NSDictionary *dict in data)
        {
           [ tableDataArray addObject:dict];
        }
        [self.tableView reloadData];
    }
}
-(void) DownloadFacultyData
{
    SVHTTPRequest *request = [[SVHTTPRequest alloc]
                              initWithAddress:@"http://shourav.com/cse-buet/get_teachers_info.php"
                              method:SVHTTPRequestMethodGET
                              parameters:nil
                              completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error){
                int  statusCode= urlResponse.statusCode;
                NSLog(@"status code: %d ",statusCode);
                if(statusCode==200)
                {
                    NSArray *arry=[NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:&error];
                    NSLog(@"%@: response type",response);
                    
                    [self LoadData:arry];
                }
                if(error){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:error.localizedDescription delegate:self cancelButtonTitle:@"Cancel"
                otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
    [request start];
}
@end
